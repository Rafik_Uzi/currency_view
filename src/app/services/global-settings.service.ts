import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
})

export class GlobalSettingsService {
    get changeCurrent(): boolean {
        return this._changeCurrent;
    }

    private _changeCurrent: boolean = false;

    setCurrentMode(value: boolean): void {
        this._changeCurrent = value;
    }
}
