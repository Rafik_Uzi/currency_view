import { Inject, Injectable, LOCALE_ID } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {  NBPData,ECBData, Flag } from '../API/server.component';
import { formatDate } from '@angular/common';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  dateString: string;

  constructor(private http: HttpClient, @Inject(LOCALE_ID) private locale: string) {
    this.dateString = formatDate(Date.now(),'yyyy-MM-dd',this.locale);
   }

   

  GetAPIData(date: Date): Observable<NBPData[]> {
    const dateString = formatDate(date, 'yyyy-MM-dd', this.locale);    
    return this.http.get<NBPData[]>('https://api.nbp.pl/api/exchangerates/tables/A/'+  dateString + '/?format=json' );
  }

  GetApiCBE(){
    return this.http.get<ECBData>('https://sdw-wsrest.ecb.europa.eu/service/data/EXR/D..EUR.SP00.A?startPeriod=2021-05-31&endPeriod=2021-05-31&format=jsondata');
  }
  GetApiFlag(){
    return this.http.get<Flag[]>('/assets/Flagconfig.json');
  }
}
