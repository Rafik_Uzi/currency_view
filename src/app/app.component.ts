import { Component,OnInit } from '@angular/core';
import { GlobalSettingsService } from './services/global-settings.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent implements OnInit {   
  title = 'TestProjekt';
  checked: boolean; 
  navbarCollapsed = true;

  constructor(private globalSettingsService: GlobalSettingsService) {

  }

  ngOnInit(): void {
    
  }
  

  onchange(){
    if(this.checked){
      document.body.classList.toggle('darkmode')
    } else {
      document.body.classList.toggle('light-theme')
    }

    this.globalSettingsService.setCurrentMode(this.checked);
    console.log(this.checked)
  }

}
