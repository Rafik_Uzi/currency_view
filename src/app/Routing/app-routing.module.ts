import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { servercomponent } from '../API/server.component';



const routes: Routes = [
  { path: '', redirectTo: 'api', pathMatch: 'full' }, // domyślan strona
  { path: 'api', component: servercomponent },
  { path: '**',  redirectTo: '' } //przekierowanie dowolny adres 
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }