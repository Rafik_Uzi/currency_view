import { Component, OnInit } from '@angular/core';
import { catchError, of } from 'rxjs';
import { HttpService } from '../services/http.service';
import { GlobalSettingsService } from '../services/global-settings.service';

export interface NBPData {
   table: string
   no: string
   effectiveDate: string
   rates: Rate[]
}

export interface Rate {
   currency: string
   code: string
   mid: string
}


export interface ECBData {
  dataSets:  DataSet[];
  structure: Structure;
}

export interface DataSet {
  action:    string;
  validFrom: Date;
  series:    { [key: string]: Series };
}

export interface Series {
  attributes:   Array<number | null>;
  observations: Observations;
}

export interface Observations {
  "0": Array<number | null>;
}

export interface Structure {
  attributes: Attributes;
}

export interface Attributes {
  series:      SeriesElement[];
}

export interface SeriesElement {
  id:     string;
  name:   string;
  values: SeriesValue[];
}

export interface SeriesValue {
  id?:  string;
  name: string;
}

export interface Flag {
  id:  string;
  img: string;
}


@Component({
  selector: 'server-api',
  templateUrl: './server.component.html',
  styleUrls: ['./server.component.css']
})

export class servercomponent implements OnInit {

  constructor(
    private http: HttpService,
    private globalSettingsService: GlobalSettingsService) { 
  }

  dane: NBPData[] = [];
  
  dane2: ECBData | null = null;
  
  flags: Flag[] | null = null
  
  datesee: boolean;

  selectedCustomer1: NBPData[] = [];

  date = new Date()



  ngOnInit(): void {
    this.http.GetApiCBE().subscribe((data: any) => {
      console.log(data.dataSets[0].series["0:1:0:0:0"].observations[0][0])
      console.log(data.structure.attributes.series[18].values[0].id)
      this.dane2 = data
      console.log(this.dane2?.structure?.attributes?.series);
    });

    this.http.GetApiFlag().subscribe((data: Flag[]) => {
      this.flags = data
      console.log(data)
    })

  }


  getRateMid(rate: Rate): string {
    if (!this.globalSettingsService.changeCurrent) {
      return rate.mid; 

    } else {
      for(let i = 0; i < this.dane2?.structure?.attributes?.series?.[18]?.values?.length; i++) {
        const currency: string =  this.dane2?.structure?.attributes?.series?.[18]?.values?.[i]?.id ?? '';
        if (rate.code === currency) {
          return this.dane2?.dataSets?.[0]?.series?.['0:' + i  +':0:0:0']?.observations?.['0']?.[0]?.toString()
        }
      }
    }
    return null;  
  }

  getFlag(code) {
    const flag = this.flags.find(o => o.id === code);
    return flag ? flag.img : ''
  }
  

  onSelect(e) {
    const date = new Date();
    this.http.GetAPIData(date)
    .pipe(
      catchError((err: any) => of([]))
    ).subscribe((data: NBPData[]) => {
      this.dane = data
    });
  }


  Minusday(e) {
    this.date.setDate(this.date.getDate() -1);

    this.date = new Date(this.date);
    
    this.http.GetAPIData(this.date)
      .pipe(
        catchError((err: any) => of([])) // stumien
      ).subscribe((data: NBPData[]) => {
        this.dane = data
      });
    console.log(this.date);
}



  AddDays(e) {
    this.date.setDate(this.date.getDate() + 1);

    this.date = new Date(this.date); // dodawanie daty zmianna instancji wynika mam y

    this.http.GetAPIData(this.date)
    .pipe(
      catchError((err: any) => of([])) // stumien
    )
      .subscribe((data: NBPData[]) => {
        this.dane = data
      });
    console.log(this.date);
}

}

